<!DOCTYPE html>
<html>
<head>
	<title>Апартмани | Хотел Силекс</title>

    @include('includes.head')


    



</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar')



    <!-- Apartments Header -->

    <div class="container-fluid">
        <div class="row apartment-header">
            <div class= "apartment-header-overlay">
                <div class="container">
                    <h1 class="header-text">Апартмани</h1>
                </div>
            </div>
        </div>
    </div>


    <!-- Apartments Details -->

    <div class="container aparments-details-section">
        <div class="row">
            <div class="col-md-8 col-sm-12">                  
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                    <div class="item active" style="height: auto;">
                        <img src="{{asset('app/images/apartment.jpg')}}"  style="width:100%;">
                    </div>

                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/apartment1.jpg')}}"  style="width:100%;">
                    </div>
                
                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/apartment2.jpg')}}" style="width:100%;">
                    </div>

                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/apartments4.jpg')}}" style="width:100%;">
                    </div>
              
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>


                <div class="room-features">
                    <h6>Големина: <span>35²</span></h6>
                    <h6>Капацитет: <span>5</span></h6>
                    <h6>Кревет: <span>Француски лежај</span></h6>
                    <h6>Карактеристики: <span>LCD, WI-FI...</span></h6>
                </div>


                <div class="room-details">
                    <p>Апартманите се составени од спална соба и дневна соба за дополнителен релаксиран одмор. Големата и просторна тераса ке ви овозможи поглед на Охридското езеро од кој ке Ви се одземе здивот.
                    <br><br>Дополнително дневната соба може да се адаптира како уште една спална соба за престој на 2 лица.</p>
                    <span><i class="fas fa-check"></i>Кујна</span>
                    <span><i class="fas fa-check"></i>Дневна</span>
                    <span><i class="fas fa-check"></i>Спална</span>
                    <span><i class="fas fa-check"></i>Бања</span>
                    <span><i class="fas fa-check"></i>Тераса</span>
                    <span><i class="fas fa-check"></i>LCD Телевизор</span>
                    <span><i class="fas fa-check"></i>Фен</span>
                    <span><i class="fas fa-check"></i>Бесплатен WI-FI</span>
                    <span><i class="fas fa-check"></i>Кабелски телевизиски канали</span>
                    <span><i class="fas fa-check"></i>Интернационална директна телефонска линија</span>
                    <span><i class="fas fa-check"></i>Клима уред</span>
                    <span><i class="fas fa-check"></i>Мини бар</span>
                    <span><i class="fas fa-check"></i>Собна услуга</span>
                    <span><i class="fas fa-check"></i>Нарачано будење</span>
                </div>
            </div>

            <!-- Accommodation filter -->

            <div class="col-md-4 col-sm-12">
                <section class="filter-area filter-area-mobile">
                    <div class="hotel-search-form-area" style="box-shadow: none !important; -webkit-transform: translateY(0);
                    -ms-transform: translateY(0);  transform: translateY(0); position: relative;">
                        <div class="container-fluid form-container" style="padding: 0 !important">
                            <div class="hotel-search-form">
                                <form method="POST" action="{{route('contact')}}"> 
                                {{ csrf_field() }}
                                    <div class="row justify-content-between align-items-end filter-div">
                                        <div class="col-md-12">
                                            <label for="checkIn">Од</label>
                                            <input type="date" class="form-control" id="checkIn" name="checkin">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="checkOut">До</label>
                                            <input type="date" class="form-control" id="checkOut" name="checkout">
                                        </div>
                                        <div class="col-md-6 ">
                                            <label for="adults">Лица</label>
                                            <select name="adults" id="adults" class="form-control">
                                                <option value="" disabled selected class="first-option">/</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </div>                                        
                                        <div class="col-md-6 check-button">
                                            <label>/</label>
                                            <button type="submit" class="form-control btn check-avability">Провери</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>                
            </div>
        </div>
    </div>



    <!-- Footer -->

    @include('includes.footer')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>