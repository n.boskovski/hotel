<!DOCTYPE html>
<html>
<head>
    <title>Restaurant | Hotel Sileks</title>

    @include('includes.head-en')



</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar-en')


        <!-- Restaurant header -->


    <div class="container-fluid">
        <div class="row restaurant-header">
            <div class= "restaurant-header-overlay">
                <div class="container">
                    <h1 class="header-text">Restaurant</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Restaurant Details -->


    <div class="container-fluid restaurant-section">
        <div class="row">
            <div class="col-md-6" data-aos="fade-up" data-aos-duration="1000" style="padding: 0">          
                <img src="{{asset('app/images/restaurant1.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 restaurant-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Cream Hall</h2>
                <p>Taste our international and national specialties made by our culinary masters in beautiful environment.</p>
            </div>
        </div>
        <div class="row restaurant-item">
            <div class="col-md-6 restaurant-description"  data-aos="fade-up" data-aos-duration="3000">
                <h2>Aperitif Bar</h2>
                <p>Our aperitif bar is ideal meeting place offering a variety of domestic and international alcoholic and non alcoholic drinks and deliciously prepared snacks.</p>
            </div>
            <div class="col-md-6" style="padding: 0" data-aos="fade-up" data-aos-duration="2000">                  
              <img src="{{asset('app/images/aperitiv1.jpg')}}"  style="width:100%;">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" data-aos="fade-up" data-aos-duration="1500" style="padding: 0">          
                <img src="{{asset('app/images/letna.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 restaurant-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Summer terrace</h2>
                <p>Summer terrace is an ideal choice to spend the summer day heat over a refreshing cocktail or try some of our traditional specialties in our menu.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 restaurant-description"  data-aos="fade-up" data-aos-duration="3000">
                <h2>Weddings</h2>
                <p>You want an unforgettable and unique wedding ceremony that will leave thousand of memories. Our dedicated wedding coordinators will assist you to make your dream come true!</p>
            </div>
            <div class="col-md-6" style="padding: 0" data-aos="fade-up" data-aos-duration="2000">                  
              <img src="{{asset('app/images/svadbi.jpg')}}"  style="width:100%;">
            </div>
        </div>
    </div>



    <!-- Footer -->

    @include('includes.footer-en')

    {{-- Preloader --}}

    <script src="{{asset('app/js/preloader.js')}}"> </script>

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>