    <div>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container nav-container" class="">
                <div class="navbar-header">
                    <button type="button"class="navbar-toggle"data-toggle="collapse"
                    data-target="#example-navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    <div class="logo">
                        <a href="{{route('index-en')}}"><img src="{{asset('app/images/logo.png')}}" alt=""></a>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="example-navbar-collapse">       
                    <ul class="nav navbar-nav navbar-right">
                        <ul class="nav navbar-nav">
                            <li><a href="{{route('index-en')}}">Home</a></li>
                            <li><a href="{{route('accommodation-en')}}">Accommodation</a></li>
                            <li><a href="{{route('restaurant-en')}}">Restaurant</a></li>
                            <li><a href="{{route('seminars-en')}}">Seminars</a></li>
                            <li><a href="{{route('animations-en')}}">Animations</a></li>
                            <li class="book-now"><a href="{{route('contact-en')}}">CONTACT</a></li>
                            <li class="mk"><a href="{{route('index')}}"><img src="{{asset('app/images/mk.png')}}"></a></li>
                            <li class="en"><a href="{{route('index-en')}}"><img src="{{asset('app/images/en.png')}}"></a></li>            
                        </ul>  
                    </ul>
                </div>
            </div>
        </nav>
    </div>