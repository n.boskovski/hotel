<meta charset="UTF-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="hotel sileks, sileks, hotel, hotel ohrid, ohrid hotels, accommodation ohrid, appartment ohrid, rooms ohrid, pool ohrid, хотел, хотел охрид, сместување охрид, апартмани охрид, соби охрид, базен охрид, охрид, хотел силекс, силекс">
<meta name="description" content="Hotel Sileks ****  is located in tourist complex St.Stefan, 4 km away from Ohrid center, on the regional road Ohrid – St.Naum, and 10 km from Ohrid airport “St.Paul The Apostle”.">
<meta name="author" content="Nenad Boshkovski">


<meta property="og:type" content="website"/>
<meta property="og:title" content="Hotel Sileks"/>    
<meta property="og:description" content="Hotel Sileks **** is located in tourist complex St.Stefan, 4 km away from Ohrid center, on the regional road Ohrid – St.Naum, and 10 km from Ohrid airport “St.Paul The Apostle”."/>
<meta property="og:image" content="{{asset('app/images/carousel2.jpg')}}"/>
<meta property="og:url" content="https://hotelsileks.mk"/>





<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<link rel="icon" type="image/ico" href="{{asset('app/images/logo.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app/css/style.css')}}">

<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
