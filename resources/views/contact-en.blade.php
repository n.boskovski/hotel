<!DOCTYPE html>
<html>
<head>
	<title>Contact | Hotel Sileks</title>

    <!-- Toastr -->
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

    @include('includes.head-en')

</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar-en')



    <!--  CONTACT -->

    <div class="container-fluid">
        <div class="row contact-header">
            <div class= "contact-header-overlay">
                <div class="container">
                    <h1 class="header-text">Contact</h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container contact">
        <div class="row contact-info-items">
            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
                <i class="fas fa-map-marker-alt"></i>
                <p>Address</p>
                <p>Sv. Stefan, Ohrid, Macedonia</p>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
                <i class="fas fa-phone"></i>
                <p>Telephone number</p>
                <p>++389 46 277 300</p>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
                <i class="fas fa-envelope-open"></i>
                <p>E-mail</p>
                <p>info@hotelsileks.mk</p>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 contact-item">
                <i class="fas fa-globe"></i>
                <p>Web</p>
                <p>www.hotelsileks.mk</p>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-6">
                <div class="map-responsive">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3007.7405181201157!2d20.80044081537644!3d41.074664279294076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1350c363fcd4e9c1%3A0x852cb5d502c23d1c!2sHotel%20Sileks!5e0!3m2!1sen!2smk!4v1582731705129!5m2!1sen!2smk" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
           <div class="col-md-6 contact-form">
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                       {{--  <button type="button" class="close" data-dismiss="alert">x</button> --}}
                        <ul>
                            @foreach($errors->all() as $error)

                                <li>{{$error}}</li>

                            @endforeach
                        </ul>

                    </div>

                @endif

{{--                 @if($message = Session::get('success'))

                    <div class="alert alert-success alert-block">                       
                        <strong> {{$message}} </strong>         
                    </div>

                @endif --}}

                <form method="POST" action="{{route('send-mail')}}">    
                    {{csrf_field()}}
                    <input name="lang" type="hidden" value="en">
                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-6 col-sm-12">
                            <input placeholder="From:" class="form-control check-in" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="checkin" value="{{$checkin}}"/>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <input placeholder="To:" class="form-control" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="checkout" value="{{$checkout}}"/ >
                        </div>
                    </div>
                    <div class="row">   
                        <div class="col-md-3 col-sm-12">
                            <select name="adults" id="adults" class="form-control">
                                 <option value="" disabled selected class="first-option">Persons</option>
                                @foreach($numPersons as $person => $p_value)

                                    <option value="{{$person}}"

                                    @if($adults == $p_value)

                                        selected

                                    @endif 

                                    @if(old('adults') == $p_value)

                                        selected
                                        
                                    @endif 

                                    >{{$p_value}}</option>

                                    @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group name">
                                <input required type="text" name="name" class="form-control" placeholder="Name" value="{{old('name')}}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group email">
                                <input required type="email" name="email" class="form-control" placeholder="E-mail" value="{{old('email')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group message">
                                <textarea required class="form-control" name="message" placeholder="Message">{{old('message')}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="send-btn text-center">
                                <button type="submit" class="btn btn-border btn-md">Send message</button>
                            </div>
                        </div>
                    </div>
                    </div>
                </form>
            </div>              
        </div>    
    </div>





    <!-- Footer -->

    @include('includes.footer-en')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

    {{-- TOASTRS --}}

     <script>
        
        @if(Session::has('success'))

            toastr.success("{{Session::get('success')}}") 

        @endif

    </script>

</body>
</html>