<!DOCTYPE html>
<html>
<head>
	<title>Сместување | Хотел Силекс</title>

    @include('includes.head')

</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar')


    <!-- Accommodation section -->


    <div class="container-fluid">
        <div class="row acc-header">
            <div class= "acc-header-overlay">
                <div class="container">
                    <h1 class="header-text">Сместување</h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container acc-section">
        <div class="row">
            <div class="col-md-8 col-sm-6" data-aos="fade-up" data-aos-duration="2000" style="margin-bottom: 30px">
                <div class="row">
                    <div class="col-md-6">
                        <div class="acc-photo acc-photo-cover">
                            <img src="{{asset('app/images/sobi.jpg')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="acc-detail">
                            <h2>                                
                                <a href="{{route('rooms')}}">Соби</a>
                            </h2>
                            <div class="col-sm-6 com-xs-6">
                                <p>Големина:
                                   <span>20²</span>
                                </p>
                               <p>Кревет:
                                   <span>Единечен/Француски лежај</span>
                                </p>
                            </div>
                           <div class="col-sm-6 com-xs-6" style="margin-bottom: 20px">
                                <p>Капацитет:
                                   <span>2</span>
                                </p>
                               <p>Карактеристики:
                                   <span>LCD, WI-FI...</span>
                                </p>
                            </div>
                            <div class="acc-read-more">
                                <a href="{{route('rooms')}}"> Повеќе детали →</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <section class="filter-area filter-area-mobile">
                    <div class="hotel-search-form-area" style="box-shadow: none !important; -webkit-transform: translateY(0);
                    -ms-transform: translateY(0);  transform: translateY(0); position: relative;">
                        <div class="container-fluid form-container" style="padding: 0 !important">
                            <div class="hotel-search-form">
                                <form method="POST" action="{{route('contact')}}"> 
                                {{ csrf_field() }}
                                    <div class="row justify-content-between align-items-end filter-div">
                                        <div class="col-md-12">
                                            <label for="checkIn">Од</label>
                                            <input type="date" class="form-control" id="checkIn" name="checkin">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="checkOut">До</label>
                                            <input type="date" class="form-control" id="checkOut" name="checkout">
                                        </div>
                                        <div class="col-md-6 ">
                                            <label for="adults">Лица</label>
                                            <select name="adults" id="adults" class="form-control">
                                                <option value="" disabled selected class="first-option">/</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </div>                                        
                                        <div class="col-md-6 check-button">
                                            <label>/</label>
                                            <button type="submit" class="form-control btn check-avability">Провери</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>                
            </div>
 
            <div class="col-md-8 col-sm-6" data-aos="fade-up" data-aos-duration="2000" style="margin-bottom: 30px">
                <div class="row">
                    <div class="col-md-6">
                        <div class="acc-photo acc-photo-cover">
                            <img src="{{asset('app/images/apartmani.jpg')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="acc-detail">
                            <h2>                                
                                <a href="{{route('apartments')}}">Апартмани</a>
                            </h2>
                            <div class="col-sm-6 com-xs-6">
                                <p>Големина:
                                   <span>35²</span>
                                </p>
                               <p>Кревет:
                                   <span>Француски лежај</span>
                                </p>
                            </div>
                           <div class="col-sm-6 com-xs-6" style="margin-bottom: 20px">
                                <p>Капацитет:
                                   <span>5</span>
                                </p>
                               <p>Карактеристики:
                                   <span>LCD, WI-FI...</span>
                                </p>
                            </div>
                            <div class="acc-read-more">
                                <a href="{{route('apartments')}}">Повеќе детали →</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-6" data-aos="fade-up" data-aos-duration="2000" style="margin-bottom: 30px">
                <div class="row">
                    <div class="col-md-6">
                        <div class="acc-photo acc-photo-cover">
                            <img src="{{asset('app/images/pretsedatelski.jpg')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="acc-detail">
                            <h2>                                
                                <a href="{{route('presidential-suite')}}">Претседателски Апартман</a>
                            </h2>
                            <div class="col-sm-6 com-xs-6">
                                <p>Големина:
                                   <span>70²</span>
                                </p>
                               <p>Кревет:
                                   <span>Француски лежај</span>
                                </p>
                            </div>
                           <div class="col-sm-6 com-xs-6" style="margin-bottom: 20px">
                                <p>Капацитет:
                                   <span>6</span>
                                </p>
                               <p>Карактеристики:
                                   <span>LCD, WI-FI...</span>
                                </p>
                            </div>
                            <div class="acc-read-more">
                                <a href="{{route('presidential-suite')}}">Повеќе детали →</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->

    @include('includes.footer')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>