<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
	'uses' => 'FrontEndController@index',
	'as' => 'index'	
]);
Route::get('/accommodation', [
	'uses' => 'FrontEndController@accommodation',
	'as' => 'accommodation'	
]);
Route::get('/restaurant', [
	'uses' => 'FrontEndController@restaurant',
	'as' => 'restaurant'	
]);
Route::get('/seminars', [
	'uses' => 'FrontEndController@seminars',
	'as' => 'seminars'	
]);
Route::get('/animations', [
	'uses' => 'FrontEndController@animations',
	'as' => 'animations'	
]);
// Route::post('/contact', [
// 	'uses' => 'FrontEndController@contact',
// 	'as' => 'contact'	
// ]);
Route::match(array('GET', 'POST'), '/contact', [
	'uses' => 'FrontEndController@contact',
	'as' => 'contact'
]);
Route::get('/accommodation/rooms', [
	'uses' => 'FrontEndController@rooms',
	'as' => 'rooms'
]);
Route::get('/accommodation/apartments', [
	'uses' => 'FrontEndController@apartments',
	'as' => 'apartments'
]);
Route::get('/accommodation/presidential-suite', [
	'uses' => 'FrontEndController@pSuite',
	'as' => 'presidential-suite'
]);

Route::get('/en', [
	'uses' => 'FrontEndController@indexEn',
	'as' => 'index-en'
]);
Route::get('/accommodation-en', [
	'uses' => 'FrontEndController@accommodationEn',
	'as' => 'accommodation-en'
]);
Route::get('/accommodation/rooms-en', [
	'uses' => 'FrontEndController@roomsEn',
	'as' => 'rooms-en'
]);
Route::get('/accommodation/apartments-en', [
	'uses' => 'FrontEndController@apartmentsEn',
	'as' => 'apartments-en'
]);
Route::get('/accommodation/presidential-suite-en', [
	'uses' => 'FrontEndController@pSuiteEn',
	'as' => 'presidential-suite-en'
]);
Route::get('/restaurant-en', [
	'uses' => 'FrontEndController@restaurantEn',
	'as' => 'restaurant-en'
]);
Route::get('/seminars-en', [
	'uses' => 'FrontEndController@seminarsEn',
	'as' => 'seminars-en'
]);
Route::get('/animations-en', [
	'uses' => 'FrontEndController@animationsEn',
	'as' => 'animations-en'
]);
// Route::post('/contact-en', [
// 	'uses' => 'FrontEndController@contactEn',
// 	'as' => 'contact-en'
// ]);

Route::match(array('GET', 'POST'), '/contact-en', [
	'uses' => 'FrontEndController@contactEn',
	'as' => 'contact-en'
]);


Route::post('/sendmail', [
	'uses' => 'FrontEndController@sendMail',
	'as' => 'send-mail'	
]);

